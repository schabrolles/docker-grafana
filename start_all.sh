#!/bin/bash

ARCH=$(uname -i)

Xvfb :0 -screen 0 1024x768x24 &

export DISPLAY=:0

cd /opt/grafana/bin

if [ ! -f ./grafana-server ]; then
    if [ -d /opt/grafana/bin/linux-$ARCH ] ; then
        cp -fp /opt/grafana/bin/linux-$ARCH/* /opt/grafana/bin
    else
        exit 1
    fi
fi

./grafana-server
